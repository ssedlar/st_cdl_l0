

from tensorflow.keras import Model

class CDL(Model):
    """Class of convolutional autoencoders."""

    """
        Attributes:
            exp_path (str): path to the experiment output

        Methods:
            name: returns object's name
    """
    def __init__(self, exp_path):
        super(CDL, self).__init__()
        """Initialize class attributes."""
        self.exp_path = exp_path

    def name(self):
        """Return object's name."""
        raise NotImplementedError()
