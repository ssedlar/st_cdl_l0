
from .. import CDL

import numpy as np

import tensorflow as tf
#tf.config.experimental_run_functions_eagerly(True)
from tensorflow.keras import Model
from tensorflow.keras.layers import Dense, Flatten, ReLU, Activation
from tensorflow.keras.constraints import max_norm, unit_norm
import os
import time
import matplotlib.pyplot as plt

class ST_CDL_L0(CDL):

    def __init__(self, n_atoms=3, lr=0.005, n_batch=100, n_epochs=1000, 
                 P_train=20, P_test=50, N_ref=20, a_S_L=204, a_T_L=256,
                 nb_max=1, i_sigma=0.0001, i_type='unifint', alpha=0.0, 
                 exp_name='exp', **kwargs):
        super(ST_CDL_L0, self).__init__(**kwargs)

        self.n_atoms = n_atoms
        self.a_S_L = a_S_L
        self.a_T_L = a_T_L
        self.i_sigma = i_sigma
        self.i_type = i_type
        self.nb_max = nb_max
        self.alpha = alpha

        self.P_train = P_train
        self.P_test = P_test
        self.N_ref = N_ref

        self.lr = lr
        self.n_batch = n_batch
        self.n_epochs = n_epochs

        self.exp_name = exp_name

        ######################################################################
        # 1. Initialization of spatial weights
        ######################################################################
        a_S_init = np.ones((self.a_S_L, self.n_atoms), dtype=np.float32)
        if self.i_type == 'unifint':
            unifint = np.random.randint(0, 2, size=(self.a_S_L, self.n_atoms))
            a_S_init += self.i_sigma * (2 * unifint.astype(np.float32) - 1)
        elif self.i_type == 'unif':
            unif = np.random.uniform(-1, 1, size=(self.a_S_L, self.n_atoms))
            a_S_init += self.i_sigma * unif.astype(np.float32)
        elif self.i_type == 'rnd':
            rnd = np.random.randn(self.a_S_L, self.n_atoms)
            a_S_init = self.i_sigma * rnd.astype(np.float32)
        a_S_init /= np.sqrt(np.sum(a_S_init**2, axis=0))

        self.a_S = tf.Variable(a_S_init, constraint=max_norm(1.01, axis=0),
                               name="spatial", trainable=True)
        ######################################################################
        # 2. Initialization of temporal weights
        ######################################################################
        if self.i_type in ['unifint', 'unif']:
            a_T_init = np.ones((self.a_T_L, self.n_atoms), dtype=np.float32)
        else:
            rnd = np.random.randn(self.a_T_L, self.n_atoms)
            a_T_init = self.i_sigma * rnd.astype(np.float32)
        a_T_init /= np.sqrt(np.sum(a_T_init**2, axis=0))

        self.a_T = tf.Variable(a_T_init, constraint=max_norm(1.01, axis=0),
                               name="temporal", trainable=True)
        ######################################################################
        # 3. Adam optimizers for each set of variables
        ######################################################################
        self.opt = tf.keras.optimizers.Adam(learning_rate=self.lr)
        ######################################################################

    def get_atom_patterns(self, db):
        ######################################################################
        # 1. Atom related patterns
        ######################################################################
        self.st_patterns = tf.einsum('ta,sa->tsa', self.a_T, self.a_S)

        w_N2 = db.w_N // 2
        w_nb_N2 = w_N2 + self.nb_max
        ######################################################################
        # 2. Selection indexing
        ######################################################################
        self.idx_s = tf.reshape(tf.range(-w_N2, w_N2), [1, -1])
        self.idx_nb_s = tf.reshape(tf.range(-w_nb_N2, w_nb_N2), [1, -1])

        self.idx_nb = tf.reshape(tf.range(-self.nb_max, self.nb_max + 1), 
                                 [1, -1])
        self.r_s_idx = tf.reshape(tf.range(2 * self.nb_max + 1), [-1, 1]) + \
                       tf.reshape(tf.range(self.a_T_L), [1, -1])

        self.zp_s_L = np.zeros((self.n_batch, w_N2, self.n_atoms))
        self.zp_s_L = tf.convert_to_tensor(self.zp_s_L.astype(np.float32))
        self.zp_s_R = np.zeros((self.n_batch, w_N2-1, self.n_atoms))
        self.zp_s_R = tf.convert_to_tensor(self.zp_s_R.astype(np.float32))

    def spat_temp_correlation(self, data):
        ######################################################################
        # 1. Correlation with spatial patterns
        ######################################################################
        sc = tf.einsum('bts,sa->bta', data, self.a_S)
        ######################################################################
        # 2. Correlation with temporal patterns
        ######################################################################
        stc = tf.nn.conv1d(sc, tf.reshape(self.a_T, [self.a_T_L, 1, self.n_atoms]), padding='VALID',stride=[1, 1, 1])

        ######################################################################
        return stc

    def get_activations(self, db, data, test=True, d='spat'):

        ######################################################################
        # 1. Find index of maximal correlations
        ######################################################################
        stc = self.spat_temp_correlation(data)
        if not self.nb_max:
            stc_v = tf.nn.relu(stc)
        else:
            stc_v = tf.nn.relu(stc[:, self.nb_max:-self.nb_max, :])

        stc_amx = tf.argmax(stc_v, axis=1, output_type=tf.int32) + self.nb_max
        stc_amx = tf.expand_dims(stc_amx, axis=2)
        ######################################################################
        # 2. Select data of interest within neighbourhood
        ######################################################################
        data_idx = stc_amx + self.a_T_L // 2 + self.idx_nb_s
        data_seg = tf.gather(data, data_idx, axis=1, batch_dims=1)
        data_seg_mse = tf.reduce_sum(data_seg ** 2, axis=[3])

        data_seg = tf.transpose(data_seg, (0, 2, 3, 1))
        data_seg = tf.gather(data_seg, self.r_s_idx, axis=1)
        data_seg_mse = tf.gather(data_seg_mse, self.r_s_idx, axis=2)
        data_seg_mse = tf.reduce_sum(data_seg_mse, axis=[3])

        ######################################################################
        # 3. Select correlation of interest with neighbourhood
        ######################################################################
        nb_idx = stc_amx + self.idx_nb
        stc_seg = tf.gather(tf.transpose(stc, (0, 2, 1)), nb_idx, axis=2, batch_dims=2)
        ######################################################################
        # 4. Compute contributions
        ######################################################################
        a = stc_seg * tf.cast(stc_seg > 0, dtype=tf.float32)
        contribs = tf.einsum('ban,tsa->bntsa', a, self.st_patterns)
        ######################################################################
        # 5. Compute activation indices and amplitudes
        ######################################################################
        diff = (data_seg - contribs)
        diff_mse = tf.reduce_sum(diff ** 2, axis=[2, 3])
        diff_mse = tf.transpose(diff_mse, (0, 2, 1))

        diff_mask = tf.cast(diff_mse < data_seg_mse, dtype=tf.float32)
        nb_amin = tf.argmin(diff_mse, axis=2, output_type=tf.int32)

        diff_mask_min = tf.gather(diff_mask, nb_amin, axis=2, batch_dims=2)

        z_pos = stc_amx[:, :, 0] + self.a_T_L // 2 + nb_amin - self.nb_max
        amp = tf.gather(a, nb_amin, axis=2, batch_dims=2) * diff_mask_min

        a_s = self.convert_to_activations(db, amp, z_pos)

        if test:
            return a_s
        ######################################################################
        # 6. Computing gradients for correlation atoms
        ######################################################################
        recon = self.decode(tf.convert_to_tensor(a_s))

        diff_idx = tf.expand_dims(z_pos, axis=2) + self.idx_s
        diff_r_seg = tf.gather(data - recon, diff_idx, axis=1, batch_dims=1)

        g = tf.einsum('nats,sa->nat', diff_r_seg, self.a_S)
        g = -2 * tf.einsum('nat,ta->na', g, self.a_T)
        g = g * diff_mask_min
        data_seg_max = tf.gather(data, diff_idx, axis=1, batch_dims=1)
        if d == 'spat':
            data_seg_max = tf.transpose(data_seg_max, (0, 1, 3, 2))
            dt = tf.einsum('bast,ta->bas', data_seg_max, self.a_T)
            g_s = tf.einsum('ba,bas->sa', g, dt) / self.n_batch
            return a_s, g_s
        elif d == 'temp':
            ds = tf.einsum('bats,sa->bta', data_seg_max, self.a_S)
            g_t = tf.einsum('ba,bta->ta', g, ds) / self.n_batch
            return a_s, g_t

    def activations_refine(self, db, data, a):

        ######################################################################
        # 1. Find index of maximal correlations
        ######################################################################
        stc = self.spat_temp_correlation(data)
        stc = tf.concat([self.zp_s_L, stc, self.zp_s_R], axis=1)

        stc_r = stc * tf.cast(a != 0, dtype=tf.float32)
        stc_r_amx = tf.argmax(tf.abs(stc_r), axis=1, output_type=tf.int32)
        stc = tf.transpose(stc, (0, 2, 1))
        a = tf.transpose(a, (0, 2, 1))
        ######################################################################
        # 2. Select data of interest
        ######################################################################
        data_r_idx = tf.expand_dims(stc_r_amx, axis=2) + self.idx_s
        data_r_seg = tf.gather(data, data_r_idx, axis=1, batch_dims=1)
        data_r_seg_mse = tf.reduce_sum(data_r_seg**2, axis=[2, 3])
        ######################################################################
        # 5. Select existing activations
        ######################################################################
        amp_s = tf.gather(a, stc_r_amx, axis=2, batch_dims=2)
        amp_r = tf.gather(stc, stc_r_amx, axis=2, batch_dims=2)
        ######################################################################
        # 5. Compute refinement contributions
        ######################################################################
        amp_r_mask_p = tf.cast((amp_s + amp_r) > 0, dtype=tf.float32)
        amp_r_mask_n = tf.cast((amp_s + amp_r) < 0, dtype=tf.float32)
        amp_r = amp_r * amp_r_mask_p
        amp_r -= amp_s * amp_r_mask_n
        r_contribs = tf.einsum('ba,tsa->bats', amp_r, self.st_patterns)
        ######################################################################
        # 6. Compute activation indices and amplitudes
        ######################################################################
        diff_r = (data_r_seg - r_contribs)
        diff_r_mse = tf.reduce_sum(diff_r ** 2, axis=[2, 3])
        mask_r = tf.cast(diff_r_mse < data_r_seg_mse, dtype=tf.float32)
        a_s = self.convert_to_activations(db, amp_r * mask_r, stc_r_amx)
        return a_s

    def decode(self, a):
        atc = tf.nn.conv1d(a, tf.reshape(self.a_T[::-1, :], [self.a_T_L, 1, self.n_atoms]), 
                           padding='SAME', stride=[1, 1, 1])
        return tf.einsum('ntc,sc->nts', atc, self.a_S)

    def convert_to_activations(self, db, A, Z):
        a_s = np.zeros((self.n_batch, db.se_N, self.n_atoms), np.float32)
        A, Z = A.numpy(), Z.numpy()
        for i in range(self.n_atoms):
            a_s[np.arange(self.n_batch), Z[:, i], i] = A[:, i]
        return tf.convert_to_tensor(a_s)

    @tf.function
    def update_weights_decoder_s(self, data, a):
        with tf.GradientTape() as tf_tape:
            atc = tf.nn.conv1d(a, tf.reshape(self.a_T[::-1, :], [self.a_T_L, 1, self.n_atoms]), 
                               padding='SAME', stride=[1, 1, 1])
            r = tf.einsum('ntc,sc->nts', atc, self.a_S)
            loss = tf.reduce_mean(tf.reduce_sum((data - r) ** 2, axis=[1, 2]))
            loss_n = tf.reduce_mean(tf.reduce_sum((data - r) ** 2, axis=[1, 2]) /
                                    tf.reduce_sum(data**2, axis=[1, 2]))
        g_s = tf_tape.gradient(loss, [self.trainable_variables[0]])
        self.opt.apply_gradients(zip(g_s, [self.trainable_variables[0]]))
        return loss, loss_n

    @tf.function
    def update_weights_decoder_t(self, data, a):

        with tf.GradientTape() as tf_tape:
            atc = tf.nn.conv1d(a, tf.reshape(self.a_T[::-1, :], [self.a_T_L, 1, self.n_atoms]), 
                               padding='SAME', stride=[1, 1, 1])
            r = tf.einsum('ntc,sc->nts', atc, self.a_S)
            loss = tf.reduce_mean(tf.reduce_sum((data - r) ** 2, axis=[1, 2]))

            loss_n = tf.reduce_mean(tf.reduce_sum((data - r) ** 2, axis=[1, 2]) /
                                    tf.reduce_sum(data**2, axis=[1, 2]))

        g_t = tf_tape.gradient(loss, [self.trainable_variables[1]])
        self.opt.apply_gradients(zip(g_t, [self.trainable_variables[1]]))
        return loss, loss_n

    @tf.function
    def update_weights_encoder(self, g, d='norm'):
        if d == 'spat':
            self.opt.apply_gradients(zip(g, [self.trainable_variables[0]]))
        elif d == 'temp':
            self.opt.apply_gradients(zip(g, [self.trainable_variables[1]]))

    def train_encoder_step(self, db, X, d='norm'):
        self.get_atom_patterns(db)
        X_h = X
        a = tf.zeros((self.n_batch, db.se_N, self.n_atoms))

        if d == 'spat':
            g = tf.zeros((self.a_S_L, self.n_atoms))
        elif d == 'temp':
            g = tf.zeros((self.a_T_L, self.n_atoms))

        for q in range(self.P_train):
            a_s, g_s = self.get_activations(db, X_h, test=False, d=d)
            if tf.reduce_sum(a_s) == 0:
                break
            a += a_s
            X_h = X - self.decode(a)
            g += g_s
        g /= self.P_train
        g *= self.alpha
        self.update_weights_encoder([g], d=d)
        loss = tf.reduce_mean(tf.reduce_sum(X_h**2, axis=[1, 2]))
        return loss

    def train_decoder_step(self, db, X, d='spat'):
        self.get_atom_patterns(db)
        X_h = X
        a = tf.zeros((self.n_batch, db.se_N, self.n_atoms))
        for q in range(self.P_train):
            a_s = self.get_activations(db, X_h)
            if tf.reduce_sum(a_s) == 0:
                break
            a += a_s
            X_h = X - self.decode(a)
        if d == 'spat':
            return self.update_weights_decoder_s(X, a)
        elif d == 'temp':
            return self.update_weights_decoder_t(X, a)


    def test(self, db, X, e_idx, losses, losses_n, mode='test'):

        output_path = os.path.join(self.exp_path, self.exp_name, self.name, mode)
        if not os.path.exists(output_path):
            os.makedirs(output_path)

        self.get_atom_patterns(db)
        X_h = X
        a = tf.zeros((X.shape[0], db.se_N, self.n_atoms))
        for q in range(self.P_test):
            a_s = self.get_activations(db, X_h)
            if tf.reduce_sum(a_s) == 0:
                break
            a += a_s
            X_h = X - self.decode(a)
            for i in range(self.N_ref):
                a_r = self.activations_refine(db, X_h, tf.convert_to_tensor(a))
                if tf.reduce_sum(a_r) == 0:
                    break
                a += a_r
                X_h = X - self.decode(a)
        r = self.decode(a)
        print(tf.reduce_mean(tf.reduce_sum((X - r)**2, axis=[1, 2]) / tf.reduce_sum(X**2, axis=[1, 2])))
        #print("estim:", np.max(np.max(r, axis=2), axis=0))
        #print("estim:", np.std(np.max(r, axis=2), axis=0))
        np.save(os.path.join(output_path, 'activations.npy'), a)
        np.save(os.path.join(output_path, 'waveforms.npy'), self.a_T.numpy())
        np.save(os.path.join(output_path, 'spatials.npy'), self.a_S.numpy())
        np.save(os.path.join(self.exp_path, mode+'_data.npy'), X.numpy())

        np.save(os.path.join(output_path, 'losses_{0}.npy').format(e_idx), 
                losses)
        np.save(os.path.join(output_path, 'losses_n_{0}.npy').format(e_idx), 
                losses)
        return a

    def run_training(self, db):

        ######################################################################
        # 1. Loading data
        ######################################################################

        X_train_tf = tf.data.Dataset.\
            from_tensor_slices(db.train_data).batch(self.n_batch)
        X_test = tf.convert_to_tensor(db.test_data)
        X_train = tf.convert_to_tensor(db.train_data)

        ######################################################################
        # 2. Model training
        ######################################################################
        losses = np.zeros(self.n_epochs+1)
        losses_n = np.zeros(self.n_epochs+1)
        c = 0
        for e_idx in range(0, self.n_epochs+1):

            l_acc = 0
            l_acc_n = 0
            time_start = time.time()

            loss, loss_n = self.train_decoder_step(db, X_train, d='spat')
            l_acc += loss
            l_acc_n += loss_n
            loss, loss_n = self.train_decoder_step(db, X_train, d='temp')
            l_acc += loss
            l_acc_n += loss_n
            if self.alpha != 0:
                l_acc += self.train_encoder_step(db, X_train, d='spat')
                l_acc += self.train_encoder_step(db, X_train, d='temp')
                l_acc /= 4
            else:
                l_acc /= 2
                l_acc_n /= 2

            losses[e_idx] = l_acc
            losses_n[e_idx] = l_acc_n

            print("loss:", e_idx, losses[e_idx], losses_n[e_idx])
            if e_idx >= 100:
                if (losses[e_idx-1] - losses[e_idx]) < 1.e-5:
                    lr_c = self.opt.lr.read_value()
                    self.opt.lr.assign(lr_c * 0.8)
                    c+=1
                else:
                    c = 0

            if c == 5 or e_idx == self.n_epochs:
                a = self.test(db, X_train, e_idx, losses, losses_n, mode='train')
                self.n_batch = X_test.shape[0]
                a = self.test(db, X_test, e_idx, losses, losses_n)
                import sys
                sys.exit(2)

            time_end = time.time()
            print("time elapsed:", time_end - time_start)

        ######################################################################
    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(exp_name=%s, lr=%s, i_sigma=%s, nb_max=%s, " \
               "P_train=%s, P_test=%s, N_ref=%s, alpha=%s)" % \
               (type(self).__name__, self.exp_name, self.lr, 
                self.i_sigma, self.nb_max, 
                self.P_train, self.P_test, self.N_ref, self.alpha)
