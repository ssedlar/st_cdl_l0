

class Database(object):
    """Class for EEG/MEG data management."""

    """
        Attributes:
            db_path (str): path to the database

        Methods:
            load_data: loads data
            name: returns object's name
    """
    def __init__(self, db_path):
        """Initialize class attributes."""
        self.db_path = db_path

    def load_data(self):
        """Loads data."""
        """
            Returns:
                (numpy array): numpy array containing data
        """
        raise NotImplementedError()

    def name(self):
        """Return object's name."""
        raise NotImplementedError()
