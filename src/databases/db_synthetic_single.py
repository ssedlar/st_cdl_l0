# Script that runs generation of EEG/MEG synthetic data

from .waveforms import *
from scipy import sparse
import time
import sys
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from mne.simulation import simulate_sparse_stc
from mne.forward import _stc_src_sel
from mne.viz import plot_sparse_source_estimates
from mayavi import mlab
import mne
from mne.datasets import sample
from mne import make_ad_hoc_cov
from mne.simulation import add_noise
from mne.bem import _fit_sphere

from .. import Database

class DatabaseSyntheticSingle(Database):

    def __init__(self, n_activ_sources=3, s_freq=128, n_trials=100, 
                 waveform_T=2, sig_T=10, 
                 random_seed=1234, activ_density=0.01, **kwargs):
        """DatabaseSynthetic attribute initialization."""
        super(DatabaseSyntheticSingle, self).__init__(**kwargs)

        self.n_activ_sources = n_activ_sources
        self.s_freq = s_freq
        self.n_trials = n_trials
        self.waveform_T = waveform_T
        self.waveform_N = waveform_T * s_freq
        self.sig_T = sig_T
        self.sig_N = sig_T * s_freq
        self.sig_ext_N = self.waveform_N + self.sig_N

        self.activ_density = activ_density
        self.random_seed = random_seed

        self.data = None
        self.waveforms_gt = None
        self.activations = None

    def load_data(self):

        # 1. Define input path
        input_path = os.path.join(self.db_path, self.name)

        self.data = np.load(os.path.join(input_path, 'data.npy'))
        self.waveforms_gt = np.load(os.path.join(input_path, 'waveforms.npy'))
        self.activations = np.load(os.path.join(input_path, 'activations.npy'))

    def generate_data(self):

        output_path = os.path.join(self.db_path, self.name)
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        if os.path.exists(os.path.join(output_path, 'done')):
            return

        # 1. Allocating arrays for data samples and ground truth
        t_sig = time_instants(t1=self.sig_T, fs=self.s_freq)
        t_wave = time_instants(t1=self.waveform_T, fs=self.s_freq)
        data = np.zeros((self.n_trials, self.sig_ext_N))
        waveforms_gt = np.zeros((self.n_activ_sources, self.waveform_N))
        activs_gt = np.zeros((self.n_activ_sources, self.sig_ext_N, self.n_trials))

        # 5. Generating random activations
        np.random.seed(self.random_seed)
        for i in range(self.n_trials):
            activs_gt[:, self.waveform_N // 2:-self.waveform_N // 2, i] = \
                sparse.random(self.n_activ_sources, self.sig_N, 
                              density=self.activ_density).toarray()

        # 6. Defining waveforms
        spike = spike_waveform(t_wave)
        spindle = gauss_weighted_sine_waveform(t_wave)
        sawtooth = sawtooth_waveform(t_wave)

        if self.n_activ_sources >= 1:
            waveforms_gt[0, :] = spike
        if self.n_activ_sources >= 2:
            waveforms_gt[1, :] = spindle
        if self.n_activ_sources == 3:
            waveforms_gt[2, :] = sawtooth

        # 10. Generate multivariable signals
        for i in range(self.n_trials):
            for j in range(self.n_activ_sources):
                x = np.convolve(activs_gt[j, :, i], waveforms_gt[j, :], 
                                mode='same')
                data[i, :] += x
                plt.figure(1)
                plt.plot(data[i, :])
                plt.show()

        # 12. Saving generated data
        np.save(os.path.join(output_path, 'data.npy'), data)
        np.save(os.path.join(output_path, 'waveforms.npy'), waveforms_gt)
        np.save(os.path.join(output_path, 'activations.npy'), activs_gt)
        with open(os.path.join(output_path, 'done'), 'a') as f:
            f.close()

    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(n_activ_sources=%s, s_freq=%s, activ_density=%s)" % \
               (type(self).__name__, self.n_activ_sources, 
                self.s_freq, self.activ_density)