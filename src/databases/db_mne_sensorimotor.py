# Script that runs generation of EEG/MEG synthetic data

from .waveforms import *
from scipy import sparse
import time
import sys
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from mne.simulation import simulate_sparse_stc
from mne.forward import _stc_src_sel
from mne.viz import plot_sparse_source_estimates
from mayavi import mlab
import mne
from mne.datasets import sample
from mne import make_ad_hoc_cov
from mne.simulation import add_noise
from mne.bem import _fit_sphere

from .. import Database

class DatabaseMNESomato(Database):
    """Class for synthetic multichannel MEG data generation."""

    """Child class of Database."""

    """
        Attributes:
            sf (float)   : sampling frequency
            s_T (float)  : signal length in seconds
            s_N (int)    : discrete signal length
            w_T (float)  : waveform length in seconds
            w_N (int)    : discrete waveform length
            se_N (int)   : length of discrete signal with zero padding
            
            n_ch (int)   : number of channels (sensors)

            n_trials (int)  : number of samples to generate

            rnd_seed (int)  : random seed for random processes
                              such as spatial selection of the sources
                              and temporal generation of the activations
            
            wave_gt (np array)    : temporal waveforms
            spat_gt (np array)    : spatial patterns (topograpic maps)

            train_data (np array) : training multichannel MEG data
            train_acts (np array) : training activations

            test_data (np array)  : testing multichannel MEG data
            test_acts (np array)  : testing activations

        Methods:
            load_data      : loads train and test data and activations
            generate_data  : generates train and test MEG data
            name           : returns name of the database
    """

    def __init__(self, sf=150, n_ch=204, w_T=0.4, s_T=6, **kwargs):
        """DatabaseSynthetic attribute initialization."""
        super(DatabaseMNESomato, self).__init__(**kwargs)

        self.sf = sf
        self.s_T = s_T
        self.s_N = 901
        self.w_T = w_T
        self.w_N = int(np.round(w_T * sf)) // 2 * 2
        self.se_N = self.s_N

        self.n_ch = n_ch

        self.n_trials = None

        self.train_data = None
        self.test_data = None

    def load_data(self):
        """Loads train and test data."""

        self.train_data = np.load(os.path.join(self.db_path, 'data.npy')).astype(np.float32).transpose(0, 2, 1)

        self.test_data = self.train_data

        self.n_trials = self.train_data.shape[0]

    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(sf=%s)" % \
               (type(self).__name__, self.sf)