
import numpy as np
from scipy import stats
from scipy.signal import sawtooth
from scipy import linalg
from scipy.signal import tukey

################################################################################
# Functions adapted from matlab code provided here:
# [1] https://github.com/hitziger/AWL/tree/master/matlabFunctions/awl_functions
# functions:
#   spike_waveforms
#   gauss_weighted_sine_waveform
#   sawtooth_waveform
# Simulation of mu shape waveform adapted from code:
# [2] https://github.com/alphacsc/alphacsc/blob/master/alphacsc/datasets/simulate.py
# functions:
#   mu_waveform
################################################################################


def time_instants(fs, t0=0, t1=5, t_ext=5, ext=False):
    """Creates regular and extended time axis given corresponding parameters."""
    """
        Arguments:
            fs: sampling frequency (Hz)
            t0: start of the regular time window (sec)
            t1: end of the regular time window (sec)
            t_ext: lenght of the left and right (sec)
            ext: bool indicating whether to extend epoch interval
        Return:
            t: regular time instants
            t_ext: extended time instants
    """
    dt = 1. / fs
    t = np.linspace(t0, t1, int((t1 - t0) / dt), endpoint=False)

    if ext:
        t_left = np.linspace(t0 - t_ext, t0, t_ext / dt, endpoint=False)
        t_right = np.linspace(t1 + dt, t1 + t_ext, t_ext / dt, endpoint=True)

        t_ext = np.concatenate([t_left, t, t_right])

        return t, t_ext
    else:
        return t


def spike_waveform(t, k=[1.5, 2], theta=[0.05, 0.25], c=[1, 1.5], norm=False,
                   scale=1):
    """Creates a spike as superposition of two gamma functions
    (negative peak and positive slow wave), approx. 2s long."""

    """
        Arguments:
            t (array): time instants
            k (list): shape parameters for the two gamma distributions 
            theta (list): scale parameters for the two gamma distributions
            c (list): contributions of gamma distributions to the spike signal
            norm (bool): flag indicating whether to perform l2 normalization
            scale (float): signal scale
        Returns:
            spike signal as an array
    """
    #offset = np.random.uniform(0.1, 3.5)
    offset = 0.3
    # fast negative peak
    k1 = k[0] #+ k[0] * np.random.uniform(-0.1, 0.1)
    theta1 = theta[0] #+ theta[0] * np.random.uniform(-0.1, 0.1)
    gamma_gen_1 = stats.gamma(k1, scale=theta1)
    spike1 = -gamma_gen_1.pdf(t - offset)

    # positive slow wave
    k2 = k[1] #+ k[1] * np.random.uniform(-0.1, 0.1)
    theta2 = theta[1] #+ theta[1] * np.random.uniform(-0.1, 0.1)
    gamma_gen_2 = stats.gamma(k2, scale=theta2)
    spike2 = gamma_gen_2.pdf(t - offset)

    c1 = c[0] #+ c[0] * np.random.uniform(-0.1, 0.1)
    c2 = c[1] #+ c[1] * np.random.uniform(-0.1, 0.1)
    spike = c1 * spike1 + c2 * spike2

    if norm:
        spike /= linalg.norm(spike)

    scale = 1./ np.max(np.abs(spike))
    return 2 * spike * scale


def gauss_weighted_sine_waveform(t, freq=6.37, sigma=0.35, norm=False,
                                 scale=1):
    """Creates spindle - sine waveform weighted with Gaussian window."""
    """
        Arguments:
            t (array): time instants
            freq (float): frequency
            sigma (float): Gaussian window variance
            norm (bool): flag indicating whether to perform l2 normalization
            scale (float): signal scale
        Returns:
            weighted sine waveform as an array
    """
    sigma_r = sigma + sigma * np.random.uniform(-0.1, 0.1)
    freq_r = freq + freq * np.random.uniform(-0.1, 0.1)

    offset = np.random.uniform(3 * sigma_r, t[-1] - 3 * sigma_r)
    offset = 1
    window = np.exp(-(t - offset) ** 2 / (2 * sigma_r ** 2))

    weighted_sine = np.cos(2 * np.pi * freq_r * t) * window

    if norm:
        weighted_sine /= linalg.norm(weighted_sine)
    scale = 1./ np.max(np.abs(weighted_sine))
    return weighted_sine * scale


def sawtooth_waveform(t, freq=2.21, norm=False, scale=1):
    """Creates sawtooth waveform."""
    """
        Arguments:
            t (array): time instants
            freq (float): frequency
            norm (bool): flag indicating whether to perform l2 normalization
            scale (float): signal scale
        Returns:
            sawtooth waveform as an array
    """

    freq_r = freq #+ freq * np.random.uniform(-0.1, 0.1)
    offset = np.random.uniform(0, 1. / freq_r)
    offset = 0
    sawtooth_ = sawtooth(2 * np.pi * freq_r * (t + offset))

    if norm:
        sawtooth_ /= linalg.norm(sawtooth_)

    scale = 1./ np.max(np.abs(sawtooth_))
    return sawtooth_ * scale


def mu_waveform(t, freq=10, sfreq=100, scale=1):
    """Simulate data with 10Hz mu-wave and 10Hz oscillations."""
    """
        Arguments:
            t (array): time instants
            freq (float): mu-wave frequency
            sfreq (float): sampling frequency
            scale (float): signal scale
        Returns:
            mu-wave as defined in [2]
    """

    phase_shift = np.random.uniform(0., 1.) * sfreq * np.pi
    L = 1. + .5 * np.random.uniform(0., 1.)
    offset = np.random.uniform(0, t[-1] - L)

    mask = (t > offset) * (t < offset + L)
    time = t * 2 * np.pi * freq
    time = time + phase_shift

    noisy_phase = .5 * np.sin(time / (3 * np.sqrt(2)))

    phi_t = time + noisy_phase
    signal = np.sin(phi_t + np.cos(phi_t) * mask)

    signal = mask * signal
    signal *= tukey(signal.shape[-1], alpha=0.05)

    return signal * scale


def main():

    t = time_instants(100)

    x = sawtooth_waveform(t)
    y = spike_waveform(t)
    z = gauss_weighted_sine_waveform(t)
    import matplotlib.pyplot as plt
    plt.figure(1)
    plt.plot(x, 'r')
    plt.plot(y, 'b')
    plt.plot(z, 'g')
    plt.figure(2)
    plt.plot(x + y + z, 'k')
    plt.show()


if __name__ == '__main__':
    main()