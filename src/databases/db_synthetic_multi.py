# Script that runs generation of EEG/MEG synthetic data

from .waveforms import *
from scipy import sparse
import time
import sys
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
from itertools import cycle

from mne.simulation import simulate_sparse_stc
from mne.forward import _stc_src_sel
from mne.viz import plot_sparse_source_estimates
from mayavi import mlab
import mne
from mne.datasets import sample
from mne import make_ad_hoc_cov
from mne.simulation import add_noise
from mne.bem import _fit_sphere

from .. import Database

class DatabaseSyntheticMulti(Database):
    """Class for synthetic multichannel MEG data generation."""

    """Child class of Database."""

    """
        Attributes:
            sf (float)   : sampling frequency
            s_T (float)  : signal length in seconds
            s_N (int)    : discrete signal length
            w_T (float)  : waveform length in seconds
            w_N (int)    : discrete waveform length
            se_N (int)   : length of discrete signal with zero padding
            
            n_ch (int)   : number of channels (sensors)

            n_a_src (int)   : number of activ sources
            a_dnst (float)  : temporal density of the activations

            n_trials (int)  : number of samples to generate

            rnd_seed (int)  : random seed for random processes
                              such as spatial selection of the sources
                              and temporal generation of the activations
            
            wave_gt (np array)    : temporal waveforms
            spat_gt (np array)    : spatial patterns (topograpic maps)

            train_data (np array) : training multichannel MEG data
            train_acts (np array) : training activations

            test_data (np array)  : testing multichannel MEG data
            test_acts (np array)  : testing activations

        Methods:
            load_data      : loads train and test data and activations
            generate_data  : generates train and test MEG data
            name           : returns name of the database
    """

    def __init__(self, n_a_src=3, sf=128, n_trials=100, w_T=2, s_T=10, 
                 rnd_seed=1234, a_dnst=0.01, **kwargs):
        """DatabaseSynthetic attribute initialization."""
        super(DatabaseSyntheticMulti, self).__init__(**kwargs)

        self.sf = sf
        self.s_T = s_T
        self.s_N = s_T * sf
        self.w_T = w_T
        self.w_N = w_T * sf
        self.se_N = self.w_N + self.s_N - 1

        self.n_ch = None

        assert n_a_src <= 3, "Maximal number of sources is 3!"
        self.n_a_src = n_a_src
        self.a_dnst = a_dnst

        self.n_trials = n_trials
        self.rnd_seed = rnd_seed

        self.ch_pos = None
        self.wave_gt = None
        self.spat_gt = None

        self.train_data = None
        self.train_acts = None

        self.test_data = None
        self.test_acts = None

    def add_noise(self, n_sigma):
        """Adds Gaussian noise to train and test data."""
        #np.random.seed(self.rnd_seed)
        n_train = np.random.randn(*self.train_data.shape).astype(np.float32)
        n_test = np.random.randn(*self.test_data.shape).astype(np.float32)

        self.train_data += n_sigma * n_train
        self.test_data += n_sigma * n_test

    def load_data(self):
        """Loads train and test data."""

        input_path = os.path.join(self.db_path, self.name)

        #self.ch_pos = np.load(os.path.join(input_path, 'ch_pos.npy'))
        
        self.wave_gt = np.load(os.path.join(input_path, 'waves.npy'))
        self.spat_gt = np.load(os.path.join(input_path, 'spatials.npy'))

        self.train_data = np.load(os.path.join(input_path, 'train_data.npy'))
        self.train_act = np.load(os.path.join(input_path, 'train_acts.npy'))

        self.test_data = np.load(os.path.join(input_path, 'test_data.npy'))
        self.test_act = np.load(os.path.join(input_path, 'test_acts.npy'))

    def generate_data(self):
        """Generates train and test data."""

        ######################################################################
        # 1. Define input and output paths
        ######################################################################
        mne_data_path = os.path.join(sample.data_path(), 'MEG', 'sample')

        output_path = os.path.join(self.db_path, self.name)
        if not os.path.exists(output_path):
            os.makedirs(output_path)
        if os.path.exists(os.path.join(output_path, 'done')):
            return
        ######################################################################
        # 2. Load forward model (leadfield matrix)
        ######################################################################
        fwd_path = mne_data_path + '/sample_audvis-meg-eeg-oct-6-fwd.fif'
        fwd = mne.read_forward_solution(fwd_path)
        fwd = mne.convert_forward_solution(fwd, surf_ori=True,
                                           force_fixed=True, use_cps=True)
        ch_grad = mne.pick_types(fwd['info'], meg='grad', exclude=[])
        fwd.pick_channels([fwd.ch_names[i] for i in ch_grad])
        self.n_ch = len(ch_grad)

        ######################################################################
        # 3. Load channel positions
        ######################################################################
        raw_path = mne_data_path + '/sample_audvis_raw.fif'
        raw = mne.io.read_raw_fif(raw_path, preload=True)
        raw.load_bad_channels()
        raw.pick_types(meg='grad', eeg=False)
        pos_meg = raw._get_channel_positions(raw.info.ch_names)

        r, c = _fit_sphere(pos_meg)

        distance = np.sqrt(np.sum((pos_meg - c) ** 2, 1))
        distance = np.mean(distance / r)
        if np.abs(1. - distance) > 0.1:
            print('Your spherical fit is poor.')

        pos_meg_c = pos_meg - c
        pos_meg_c /= \
            np.expand_dims(np.sqrt(np.sum(pos_meg_c ** 2, axis=1)), axis=1)
        ######################################################################
        # 4. Allocating arrays for ground truth spatial maps and waveforms and
        #    train and test data samples and activations
        ######################################################################
        waves = np.zeros((self.n_a_src, self.w_N), dtype=np.float32)
        spatials = np.zeros((self.n_ch, self.n_a_src), dtype=np.float32)

        train_data = np.zeros((self.n_trials, self.se_N, self.n_ch), 
                              dtype=np.float32)
        train_acts = np.zeros((self.n_a_src, self.se_N, self.n_trials),
                              dtype=np.float32)
        test_data = np.zeros((self.n_trials, self.se_N, self.n_ch),
                             dtype=np.float32)
        test_acts = np.zeros((self.n_a_src, self.se_N, self.n_trials),
                             dtype=np.float32)
        ######################################################################
        # 5. Generating random activations
        ######################################################################
        np.random.seed(self.rnd_seed)

        for i in range(self.n_trials):
            train_acts[:, self.w_N // 2 - 1:-self.w_N // 2, i] = \
                sparse.random(self.n_a_src, self.s_N, self.a_dnst).toarray()
            test_acts[:, self.w_N // 2 - 1:-self.w_N // 2, i] = \
                sparse.random(self.n_a_src, self.s_N, self.a_dnst).toarray()

        '''
        for i in range(self.n_trials):
            train_acts[:, :, i] = \
                sparse.random(self.n_a_src, self.se_N, self.a_dnst).toarray()
            test_acts[:, :, i] = \
                sparse.random(self.n_a_src, self.se_N, self.a_dnst).toarray()
        '''
        '''
        for i in range(self.n_trials):
            for j in range(3):
                idx = np.random.choice(100, 1) + 128 + j * 420
                train_acts[j, idx[0], i] = 1 + 0.1 * np.random.randn(1)
                idx = np.random.choice(100, 1) + 128 + j * 420
                test_acts[j, idx[0], i] = 1 + 0.1 * np.random.randn(1)

                idx = np.random.choice(100, 1) + 128 + (3-j-1) * 420
                train_acts[j, idx[0], i] = 1 + 0.1 * np.random.randn(1)
                idx = np.random.choice(100, 1) + 128 + (3-j-1) * 420
                test_acts[j, idx[0], i] = 1 + 0.1 * np.random.randn(1)
        '''
        ######################################################################
        # 6. Defining waveforms
        ######################################################################
        t_w = time_instants(t1=self.w_T, fs=self.sf)
        spike = spike_waveform(t_w)
        spindle = gauss_weighted_sine_waveform(t_w)
        sawtooth = sawtooth_waveform(t_w)

        if self.n_a_src >= 1:
            waves[0, :] = spike / np.sqrt(np.sum(spike**2))
        if self.n_a_src >= 2:
            waves[1, :] = spindle / np.sqrt(np.sum(spindle**2))
        if self.n_a_src == 3:
            window = np.ones(256)
            window[0:8] = 0
            window[-8:] = 0
            sawtooth *= window
            waves[2, :] = sawtooth / np.sqrt(np.sum(sawtooth**2))
        ######################################################################
        # 7. Choosing source locations and 
        # getting corresponding topographic maps to sensors
        ######################################################################
        n = [0]
        def f_wave(times):
            wave = waves[n[0], :]
            n[0] += 1
            return wave
        stc = simulate_sparse_stc(fwd['src'], n_dipoles=self.n_a_src, 
                                  times=t_w, data_fun=f_wave,
                                  random_state=1234)
        src_sel, stc_sel, _ = _stc_src_sel(fwd['src'], stc)
        for i in range(self.n_a_src):
            spatials[:, i] = fwd['sol']['data'][:, src_sel[i]]
            spatials[:, i] /= np.sqrt(np.sum(spatials[:, i]**2))
        ######################################################################
        # 8. Visualization and saving spatial topographic maps and waveforms
        # and source and sensor locations
        ######################################################################
        fig = plt.figure()
        axes = fig.subplots(nrows=2, ncols=self.n_a_src)
        colors = cycle(plt.rcParams.get('axes.prop_cycle').by_key()['color'])
        for i in range(self.n_a_src):
            mne.viz.plot_topomap(spatials[:, i], fwd['info'], 
                                 axes=axes[0, i], sphere=(0, 0, 0, 0.17), 
                                 show=False)
            axes[1, i].plot(t_w, waves[i, :], c = next(colors))
        plt.savefig(os.path.join(output_path, 'spat_temp.png'))
        raw.plot_sensors(ch_type='grad', show_names=False, 
                         sphere=(0, 0, 0, 0.17))
        plt.savefig(os.path.join(output_path, 'sensors.png'))
        plot_sparse_source_estimates(fwd['src'], stc,
                                     bgcolor=(1, 1, 1), opacity=0.3, 
                                     high_resolution=True,
                                     scale_factors=[2., 2., 2.],
                                     show=False)
        mlab.savefig(os.path.join(output_path, 'activ_sources.png'))
        plt.savefig(os.path.join(output_path, 'activ_waveforms.png'))
        ######################################################################
        # 9. Generate multivariable MEG train and test data
        ######################################################################
        for i in range(self.n_trials):
            for j in range(self.n_a_src):
                s = np.convolve(train_acts[j, :, i], waves[j, :], mode='same')
                train_data[i, :, :] += np.einsum('j,i->ji', s, spatials[:, j])
                s = np.convolve(test_acts[j, :, i], waves[j, :], mode='same')
                test_data[i, :, :] += np.einsum('j,i->ji', s, spatials[:, j])
        ######################################################################
        # 10. Saving generated data
        ######################################################################
        np.save(os.path.join(output_path, 'ch_pos.npy'), pos_meg_c)

        np.save(os.path.join(output_path, 'waves.npy'), waves)
        np.save(os.path.join(output_path, 'spatials.npy'), spatials)

        np.save(os.path.join(output_path, 'train_data.npy'), train_data)
        np.save(os.path.join(output_path, 'train_acts.npy'), train_acts)

        np.save(os.path.join(output_path, 'test_data.npy'), test_data)
        np.save(os.path.join(output_path, 'test_acts.npy'), test_acts)

        with open(os.path.join(output_path, 'done'), 'a') as f:
            f.close()

    @property
    def name(self):
        """Return object's name including parameters."""
        return "%s(n_a_src=%s, sf=%s, a_dnst=%s)" % \
               (type(self).__name__, self.n_a_src, self.sf, self.a_dnst)
