import os
import sys

import argparse
from src.databases import DatabaseSyntheticMulti
from src.conv_autoencoders import ConvAE_MultiTied2

def main():

    parser = argparse.ArgumentParser(description=
    	                             'Runs experiment on synthetic data.')
    ######################################################################
    parser.add_argument('-exp_path', dest='exp_path', required=True,
                        type=str, help='Path to experiment output.')
    parser.add_argument('-exp_name', dest='exp_name', required=True,
                        type=str, help='Experiment name.')
    ######################################################################
    parser.add_argument('-n_sigma', dest='n_sigma', required=True,
                        type=float, help='Gaussian noise std.')
    ######################################################################
    parser.add_argument('-lr', dest='lr', required=True, 
                        type=float, help='Learning rate.')
    parser.add_argument('-i_sigma', dest='i_sigma', required=True,
                        type=float, help='Initialization weight std.')
    parser.add_argument('-nb_max', dest='nb_max', required=True, 
                        type=int, help='Width of neighbourhood search.')
    ######################################################################
    parser.add_argument('-P_train', dest='P_train', required=True, 
                        type=int, help='Maximal training sparsity.')
    parser.add_argument('-P_test', dest='P_test', required=True, 
                        type=int, help='Maximal testing sparsity.')
    parser.add_argument('-N_ref', dest='N_ref', required=True, 
                        type=int, help='Number of refinement steps.')
    ######################################################################
    args = parser.parse_args()

    if not os.path.exists(args.exp_path):
    	os.makedirs(args.exp_path)

    # 1. Initialize database object and load data
    db_path = os.path.join(args.exp_path, 'database')
    db = DatabaseSyntheticMulti(db_path=db_path)

    if not os.path.exists(os.path.join(db.db_path, 'done')):
    	db.generate_data()
    db.load_data()
    if args.n_sigma > 0:
        db.add_noise(n_sigma=args.n_sigma)

    # 2. Initialize convolutional autoencoder and run training
    exp_path = os.path.join(args.exp_path, 'results(n_sigma={0})').format(args.n_sigma)

    cae = ConvAE_MultiTied2(exp_path=exp_path, exp_name=args.exp_name,
                       lr=args.lr, i_sigma=args.i_sigma, 
                       nb_max=args.nb_max, 
                       P_train=args.P_train, P_test=args.P_test, N_ref=args.N_ref)

    if not os.path.exists(os.path.join(cae.exp_path, 'done')):
    	cae.run_training(db)

if __name__ == '__main__':
	main()