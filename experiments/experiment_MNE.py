import os
import sys

import argparse
from src.databases import DatabaseMNESomato
from src.conv_autoencoders import ConvAE_MultiTied

def main():

    parser = argparse.ArgumentParser(description=
    	                             'Runs experiment on synthetic data.')
    ######################################################################
    parser.add_argument('-data_path', dest='data_path', required=True,
    	                type=str, help='Path to database')
    parser.add_argument('-exp_path', dest='exp_path', required=True,
                        type=str, help='Path to experiment output.')
    parser.add_argument('-exp_name', dest='exp_name', required=True,
                        type=str, help='Experiment name.')
    parser.add_argument('-w_T', dest='w_T', required=True,
    	                type=float, help='Waveform length.')
    parser.add_argument('-n_atoms', dest='n_atoms', required=True,
    	                type=int, help='Number of atoms.')
    ######################################################################
    parser.add_argument('-lr', dest='lr', required=True, 
                        type=float, help='Learning rate.')
    parser.add_argument('-i_sigma', dest='i_sigma', required=True,
                        type=float, help='Initialization weight std.')
    parser.add_argument('-nb_max', dest='nb_max', required=True, 
                        type=int, help='Width of neighbourhood search.')
    ######################################################################
    parser.add_argument('-P_train', dest='P_train', required=True, 
                        type=int, help='Maximal training sparsity.')
    parser.add_argument('-P_test', dest='P_test', required=True, 
                        type=int, help='Maximal testing sparsity.')
    parser.add_argument('-N_ref', dest='N_ref', required=True, 
                        type=int, help='Number of refinement steps.')
    ######################################################################
    args = parser.parse_args()

    if not os.path.exists(args.exp_path):
    	os.makedirs(args.exp_path)

    # 1. Initialize database object and load data
    db = DatabaseMNESomato(db_path=args.data_path, w_T=args.w_T)

    db.load_data()

    # 2. Initialize convolutional autoencoder and run training
    exp_path = os.path.join(args.exp_path, 'results_mne(w_T={0}, n_atoms={1})')
    exp_path = exp_path.format(args.w_T, args.n_atoms)

    cae = ConvAE_MultiTied(exp_path=exp_path, exp_name=args.exp_name, n_batch=db.n_trials,
    	                   a_T_L=db.w_N, a_S_L=db.n_ch, n_atoms=args.n_atoms,
                           lr=args.lr, i_sigma=args.i_sigma, 
                           nb_max=args.nb_max,
                           P_train=args.P_train, P_test=args.P_test, N_ref=args.N_ref)

    if not os.path.exists(os.path.join(cae.exp_path, 'done')):
    	cae.run_training(db)

if __name__ == '__main__':
	main()