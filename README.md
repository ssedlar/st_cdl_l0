# Spatio-temporal convolutional dictionary learning penalized by an L0 norm

Tensorflow implementation of rank-1 spatio-temporal convolutional dictionary learning with L0 constraint 

![waveform_learning](.waveform_learning_architecture.png)

## Description:


## Dependences:
* . python 3.6.13      
* . matplotlib         
* . tensorflow 2.6.2    
* . mne-hcp             

## Stand-alone example:

